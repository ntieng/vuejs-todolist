# vue_crash_todolist

## Prerequisite
```
npm install -g @vue/cli
```

## Project setup
```
npm install
```

## Open Vue UI
```
vue ui
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
